<?php

namespace InstagramApiBundle\Controller;

use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use InstagramApiBundle\Entity\News;

class DefaultController extends Controller
{
    public function newsAction(Request $request)
    {
        //kullan�c�dan al�nan haber say�s�
        $number = $request->query->get('number');

        //nytimes apisine, son 30 g�n i�indeki en�ok payla��lan teknoloji haberlerine istek yap�l�yor.
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $query = array(
            "api-key" => "cce51674130a45da871a264e4352063a"
        );
        curl_setopt($curl, CURLOPT_URL,
            "https://api.nytimes.com/svc/mostpopular/v2/mostshared/Technology/30.json" . "?" . http_build_query($query)
        );
        $response = curl_exec($curl);


        //json format�nda d�nen veriler diziye d�n��t�r�l�yor.
        $results = json_decode(curl_exec($curl),true);
        reset($results);

        //haberlerin baz� �zelliklerinin veritaban�na kaydedilmesi.
        foreach($results['results'] as $result)
        {
            $em = $this->getDoctrine()->getManager();

            $post = new News();
            $post->setUrl($result['url']);
            $post->setAbstract($result['abstract']);
            $post->setAssetId($result['asset_id']);
            $post->setByline($result['byline']);
            $post->setTotalShares($result['total_shares']);
            $post->setTitle($result['title']);
            $em->persist($post);
            $em->flush();
        }

        //database'den kullan�c�n�n istedi�i say�da haberin �ekilmesi
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('
        SELECT t
        FROM InstagramApiBundle:News t
    ');
        $query->setMaxResults($number);
        $news = $query->getResult();
        return $this->render('InstagramApiBundle:Default:message.html.twig', array('data'=>$news));

    }

    public function indexAction()
    {
        return $this->render('InstagramApiBundle:Default:index.html.twig');
    }

}
